package com.example.tplabov;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;

public class MyThread extends Thread{

    public Handler handler;
    public String url;
    public int isImage;
    public int position;

    public MyThread(Handler handler, String url, int isImage, int position) {
        this.handler = handler;
        this.url = url;
        this.isImage = isImage;
        this.position = position;
    }

    @Override
    public void run(){
        String response = null;
        byte[] byteResponse = null;

        HttpConnection myConnection = new HttpConnection(url);
        Message message = new Message();
        message.arg1=this.isImage;
        message.arg2=this.position;
        try {
            if (isImage == 1){
                byteResponse = myConnection.getBytesDataByGET();
                message.obj = byteResponse;
            }else {
                response = myConnection.getStrDataByGET();
                message.obj = XmlParser.obtenerNoticia(response);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        this.handler.sendMessage(message);
    }
}
