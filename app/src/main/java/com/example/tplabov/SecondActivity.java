package com.example.tplabov;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent i = getIntent();

        WebView wv = super.findViewById(R.id.wView);
        WebSettings ws = wv.getSettings();
        ws.setJavaScriptEnabled(true);
        wv.loadUrl(i.getStringExtra(MyListener.VALOR_LINK));

        FloatingActionButton compartir = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        compartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_TEXT,MyListener.VALOR_LINK);
                String shareBody ="body";
                String shareSub = "sub";
                //  startActivity(Intent.createChooser(myIntent),"Compartir Usando");
                startActivity(myIntent);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(i.getStringExtra(MyListener.VALOR_TITULO));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
