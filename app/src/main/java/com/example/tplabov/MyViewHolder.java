package com.example.tplabov;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {

    TextView tvTitle;
    TextView tvDescription;
    TextView tvDate;
    ImageView ivImage;
    int position;
    MyListener listener;

    public MyViewHolder(@NonNull View itemView, MyListener listener) {
        super(itemView);
        this.listener = listener;
        this.tvTitle       = (TextView) itemView.findViewById(R.id.tvTitle);
        this.tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
        this.tvDate        = (TextView) itemView.findViewById(R.id.tvFecha);
        this.ivImage       = (ImageView) itemView.findViewById(R.id.ivImage);
        itemView.setOnClickListener(this);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        this.listener.onItemClick(this.position,v);
    }
}
