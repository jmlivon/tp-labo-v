package com.example.tplabov;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SearchView;

import java.util.List;

public class MyListener implements View.OnClickListener, MyOnItemClick, SearchView.OnQueryTextListener, DialogInterface.OnClickListener {

    public final static String VALOR_TITULO = "TITULO";
    public final static String VALOR_LINK = "LINK";

    Activity activity;
    List<Noticia> noticias;
    MyAdapter myAdapter;
    View view;

    public MyListener(){}

    public MyListener(View v){
        this.view = v;
    }

    public MyListener(Activity activity) {
        this.activity = activity;
    }

    public MyListener(Activity activity, List<Noticia> noticias){
        this.activity = activity;
        this.noticias = noticias;
    }

    public void setAdapter(MyAdapter adapter) {
        this.myAdapter = adapter;
    }

    public void setNoticias(List<Noticia> noticias) {
        this.noticias = noticias;
    }

    @Override
    public void onClick(View v) {
        /*if(v.getId() == R.id.btnIr)
        {
            Intent i = new Intent(activity,SecondActivity.class);
            i.putExtra(VALOR_STRING,"Dato desde otra activity");
            activity.startActivity(i);
        }

        if(v.getId() == R.id.btnVolver) {
            activity.finish();
        }*/
    }

    @Override
    public void onItemClick(int position, View v) {
        Log.d("Position: ",String.valueOf(position));
        Noticia n = this.noticias.get(position);

        Intent i = new Intent(activity,SecondActivity.class);
        i.putExtra(VALOR_LINK,n.getLink());
        i.putExtra(VALOR_TITULO,n.getTitle());
        activity.startActivity(i);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //Log.d("Text query",query);
        this.myAdapter.filter(query);
        this.myAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //Log.d("Text newText",newText);
        this.myAdapter.filter(newText);
        return true;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        if (which== AlertDialog.BUTTON_POSITIVE)
        {
            View v = MainActivity.view.findViewById(R.id.linearDialog);
            SharedPreferences.Editor e= MainActivity.shared.edit();
            for (View vi:v.getTouchables())
            {
                CheckBox cb=(CheckBox)vi;
                e.putBoolean(cb.getText().toString(),cb.isChecked());
            }
            e.commit();
            MainActivity.goThread();
        }

              /*Log.d("Click","Click en " + which);
        View v = this.view.findViewById(R.id.linearDialog);
        //SharedPreferences.Editor e = getPreferences(Context.MODE_PRIVATE).edit();
        for (View vi: v.getTouchables()) {
            CheckBox cb = (CheckBox)vi;
        }*/

    }
}
