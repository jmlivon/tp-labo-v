package com.example.tplabov;

import android.view.View;

public interface MyOnItemClick {
    void onItemClick(int position, View v);
}
