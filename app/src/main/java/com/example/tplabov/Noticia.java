package com.example.tplabov;

import android.graphics.Bitmap;

import java.util.Date;

public class Noticia {
    private String title;
    private String link;
    private String description;
    private Date pubDate;
    private String image;
    private byte[] bytesImage;
    private boolean hasImage;

    public Noticia() {
        this.hasImage = false;
    }

    public Noticia(String title, String link, String description, Date pubDate, String image, byte[] bytesImage, Boolean hasImage) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
        this.image = image;
        this.bytesImage = bytesImage;
        this.hasImage = false;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public byte[] getBytesImage() {
        return bytesImage;
    }

    public void setBytesImage(byte[] bytesImage) {
        this.bytesImage = bytesImage;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }
}
