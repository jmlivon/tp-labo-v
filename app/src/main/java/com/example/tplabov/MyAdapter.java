package com.example.tplabov;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> implements Filterable {

    List<Noticia> noticias;
    List<Noticia> noticiasFull;
    MyListener listener;
    Context context;
    List<Noticia> noticiasFilter;
    Handler handler;
    //CustomFilter mFilter;

    public MyAdapter(List<Noticia>noticias, MyListener listener, Context context, Handler handler){
        this.noticias = noticias;
        this.listener = listener;
        this.context = context;
        this.noticiasFilter = new ArrayList();
        this.noticiasFilter.addAll(noticias);
        this.handler = handler;
        //this.mFilter = new CustomFilter((ListAdapter) MyAdapter.this);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v =null;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_layout,viewGroup,false);

        MyViewHolder myViewHoleder = new MyViewHolder(v,this.listener);
        return myViewHoleder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Noticia noticia = this.noticias.get(i);
        myViewHolder.tvTitle.setText(noticia.getTitle());
        myViewHolder.tvDescription.setText(noticia.getDescription());
        myViewHolder.tvDate.setText(noticia.getPubDate().toString());
        myViewHolder.setPosition(i);

        /*RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));

        Glide.with(context)
                .load(noticia.getImage())
                .apply(requestOptions)
                .into(myViewHolder.ivImage);*/

        if (noticia.getHasImage() == true){
            myViewHolder.ivImage.setImageBitmap(BitmapFactory.decodeByteArray(noticia.getBytesImage(),0,noticia.getBytesImage().length));
        }else {
            MyThread myThread = new MyThread(this.handler,noticia.getImage(),1,i);
            myThread.start();
        }
    }

    @Override
    public int getItemCount() {
        return this.noticias.size();
    }

    public void setNoticias(List<Noticia>noticia){

        this.noticias=noticia;
        this.noticiasFull = new ArrayList<>(noticia);
    }

    public void setImagePer(byte[] imagen, int position){
        Noticia noticia = this.noticias.get(position);
        noticia.setBytesImage(imagen);
        noticia.setHasImage(true);
    }

    public void filter(String text){

        this.noticias.clear();

        if (text.length() > 3){
            text = text.toLowerCase();
            for (Noticia noti : this.noticiasFull) {
                if (noti.getTitle().toLowerCase().contains(text)){
                    this.noticias.add(noti);
                }
            }
        }else{
            this.noticias.addAll(this.noticiasFull);
        }

        notifyDataSetChanged();
    }


    @Override
    public Filter getFilter() {
        return null;//this.mFilter;
    }

    /*public class CustomFilter extends Filter {
        private ListAdapter listAdapter;

        private CustomFilter(ListAdapter listAdapter) {
            super();
            this.listAdapter = listAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            noticiasFilter.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                noticiasFilter.addAll(noticias);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Noticia noticia : noticias) {
                    if (noticia.getTitle().toLowerCase().contains(filterPattern)) {
                        noticiasFilter.add(noticia);
                    }
                }
            }
            results.values = noticiasFilter;
            results.count = noticiasFilter.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.listAdapter.notify();
        }
    }*/
}
