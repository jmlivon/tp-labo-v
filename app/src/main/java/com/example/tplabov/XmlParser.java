package com.example.tplabov;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class XmlParser {

    public static List<Noticia> obtenerNoticia(String xml){

        List<Noticia> noticias = new ArrayList<Noticia>();
        Noticia noticia = null;

        XmlPullParser xmlPullParser = Xml.newPullParser();
        try {
            xmlPullParser.setInput(new StringReader(xml));
            int event = xmlPullParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT){

                switch (event){

                    case XmlPullParser.START_TAG:
                        if ("item".equals(xmlPullParser.getName())){
                            noticia = new Noticia();
                        }

                        if(noticia != null){

                            if ("title".equals(xmlPullParser.getName())){
                                noticia.setTitle(xmlPullParser.nextText());
                            }
                            if ("description".equals(xmlPullParser.getName())){
                                noticia.setDescription(xmlPullParser.nextText());
                            }
                            if ("pubDate".equals(xmlPullParser.getName())){
                                DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZZZZ", Locale.ENGLISH);
                                Date newDate = dateFormat.parse(xmlPullParser.nextText());
                                //Log.d("Fecha", newDate.toString());
                                noticia.setPubDate(newDate);
                            }

                            if ("link".equals(xmlPullParser.getName())){
                                noticia.setLink(xmlPullParser.nextText());
                            }

                            if ("enclosure".equals(xmlPullParser.getName())){
                                noticia.setImage(xmlPullParser.getAttributeValue(null,"url"));
                            }
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        if("item".equals(xmlPullParser.getName())&&noticia!=null){
                            noticias.add(noticia);
                        }
                        break;
                }
                event = xmlPullParser.next();
            }
            return  noticias;

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
