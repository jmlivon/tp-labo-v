package com.example.tplabov;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

public class MyMsg extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater li = LayoutInflater.from(this.getContext());
        View v = li.inflate(R.layout.dialog_layout,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setMessage("");
        builder.setTitle("Seleccionar páginas");
        MyListener listener = new MyListener(v);
        //String[] items = new String[]{"rss1","rss2","rss3"};
        //builder.setItems(items,listener);
        builder.setPositiveButton("Ok",listener);
        builder.setNegativeButton("Cancel",listener);
        //builder.setNeutralButton("Neutral",listener);
        builder.setView(v);
        MainActivity.view=v;

        SharedPreferences.Editor e= MainActivity.shared.edit();
        for (View vi:MainActivity.view.findViewById(R.id.linearDialog).getTouchables())
        {
            CheckBox cb=(CheckBox)vi;

            cb.setChecked(MainActivity.shared.getBoolean(cb.getText().toString(),true));
        }

        return builder.create();
    }
}
