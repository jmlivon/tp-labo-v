package com.example.tplabov;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MenuCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Handler.Callback {

    private RecyclerView myRecycler;
    private MyAdapter myAdapter;
    //private List<Noticia> noticias;
    private MyListener myListener;
    //private Handler handler;

    public static View view;
    public static SharedPreferences shared ;
    public static List<String> urls;
    public static List<Noticia> noticias;
    public static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity.shared=getPreferences(Context.MODE_PRIVATE);

        urls= new ArrayList<>();
        urls.add("https://www.clarin.com/rss/deportes/futbol/");
        urls.add("https://www.clarin.com/rss/politica/");
        urls.add("https://www.clarin.com/rss/policiales/");
        urls.add("https://www.clarin.com/rss/tecnologia/");

        MainActivity.noticias = new ArrayList<Noticia>();
        MainActivity.handler = new Handler(this);
        //MyThread myThread = new MyThread(MainActivity.handler, "https://www.clarin.com/rss/deportes/futbol/",0,0);
        //myThread.start();

        for (String s:MainActivity.urls)
        {
            if (MainActivity.shared.getBoolean(s,true))
            {
                MyThread myThread = new MyThread(MainActivity.handler,s,0,0);
                myThread.start();
            }
        }

        myRecycler = (RecyclerView) findViewById(R.id.my_recycler);
        myRecycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        myRecycler.setLayoutManager(linearLayoutManager);

        this.myListener = new MyListener(this,MainActivity.noticias);

        myAdapter = new MyAdapter(MainActivity.noticias,this.myListener,this,MainActivity.handler);
        this.myListener.setAdapter(this.myAdapter);
        myRecycler.setAdapter(myAdapter);

        /*Button b = (Button) findViewById(R.id.btnIr);
        b.setOnClickListener(this.myListener);]*/
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.arg1 == 0){
            myAdapter.setNoticias((List<Noticia>)msg.obj);
            MainActivity.noticias = (List<Noticia>)msg.obj;
            this.myListener.setNoticias(MainActivity.noticias);
            myAdapter.notifyDataSetChanged();
        }else {
            this.myAdapter.setImagePer((byte[])msg.obj,msg.arg2);
            myAdapter.notifyItemChanged(msg.arg2);
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        MenuItem mi = menu.findItem(R.id.opcion2);
        SearchView sv = (SearchView) mi.getActionView();
        sv.setOnQueryTextListener(this.myListener);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.opcion1){
            MyMsg miMensaje = new MyMsg();
            miMensaje.show(getSupportFragmentManager(),"dialogo");
        }else if(item.getItemId() == R.id.opcion2){
            Log.d("Click menu","Click opción 2: " + item.getTitle());
        }
        return super.onOptionsItemSelected(item);
    }

    public static void goThread()
    {
        MainActivity.noticias.clear();
        for (String s:MainActivity.urls)
        {
            if (MainActivity.shared.getBoolean(s,true))
            {
                MyThread t = new MyThread(MainActivity.handler,s,0,0);
                t.start();
            }
        }
    }
}
